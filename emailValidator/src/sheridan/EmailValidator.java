package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {

	private static final String regex = "[a-z]{1}[a-z0-9]{2,}@([a-zA-Z\\.])?[a-z0-9]{3,}\\.[a-zA-Z]{2,}";
  
	public static void main(String args[]) {

	}
	
	public static boolean checkEmail(String email){
		Pattern pattern = Pattern.compile(regex);  
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
}

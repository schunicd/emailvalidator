package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class testEmailValidator {

	@Test
	public void testIsEmailInCorrectFormat() {
		boolean isFormatted = EmailValidator.checkEmail("account@domain.com");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testEmailIsNotInCorrectFormat() {
		boolean isFormatted = EmailValidator.checkEmail("accountdomain@.com");
		assertFalse("Unable to validate email", isFormatted);
	}

	@Test
	public void testEmailHasOnlyOneAtSymbol() {
		boolean isFormatted = EmailValidator.checkEmail("account@domain.com");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testEmailHasNoAtSymbol() {
		boolean isFormatted = EmailValidator.checkEmail("accountdomain.com");
		assertFalse("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testEmailHasMultipleAtSymbol() {
		boolean isFormatted = EmailValidator.checkEmail("account@@domain.com");
		assertFalse("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testAccountNameShouldHaveThreeLowercaseAlphabetCharacters() {
		boolean isFormatted = EmailValidator.checkEmail("account@domain.com");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testAccountNameShouldHaveThreeLowercaseAlphabetCharactersBoundaryOut() {
		boolean isFormatted = EmailValidator.checkEmail("ac@domain.com");
		assertFalse("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testAccountNameShouldHaveThreeLowercaseAlphabetCharactersBoundaryIn() {
		boolean isFormatted = EmailValidator.checkEmail("acc@domain.com");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testAccountNameShouldNotStartWithANumber() {
		boolean isFormatted = EmailValidator.checkEmail("account@domain.com");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testAccountNameShouldNotStartWithANumberBoundaryIn() {
		boolean isFormatted = EmailValidator.checkEmail("a3count@domain.com");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testAccountNameShouldNotStartWithANumberBoundaryOut() {
		boolean isFormatted = EmailValidator.checkEmail("1account@domain.com");
		assertFalse("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testDomainNameShouldHaveAtleastThreeLowercaseLettersOrNumbers() {
		boolean isFormatted = EmailValidator.checkEmail("account@abc123.com");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testDomainNameShouldHaveAtleastThreeLowercaseLettersOrNumbersBoundaryIn() {
		boolean isFormatted = EmailValidator.checkEmail("account@1bc.com");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testDomainNameShouldHaveAtleastThreeLowercaseLettersOrNumbersBoundaryOut() {
		boolean isFormatted = EmailValidator.checkEmail("account@ab.com");
		assertFalse("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testExtensionNameShouldHaveAtleastTwoLetters() {
		boolean isFormatted = EmailValidator.checkEmail("account@abc.com");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testExtensionNameShouldHaveAtleastTwoLettersBoundaryIn() {
		boolean isFormatted = EmailValidator.checkEmail("account@abc.ca");
		assertTrue("Unable to validate email", isFormatted);
	}
	
	@Test
	public void testExtensionNameShouldHaveAtleastTwoLettersBoundaryOut() {
		boolean isFormatted = EmailValidator.checkEmail("account@abc.c");
		assertFalse("Unable to validate email", isFormatted);
	}
	
}
